$(document).ready(function(){
  $('.button-collapse').sideNav();
  $('.modal-trigger').leanModal();
});
$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

$(function() {
  $('#tanggal_lahir').bootstrapMaterialDatePicker({ format : 'DD-MM-YYYY', clearButton: true, time: false });
  $('#in_time_start').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
  $('#in_time_end').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
  $('#in_time_late').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
  $('#out_time_start').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
  $('#out_time_end').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
});
