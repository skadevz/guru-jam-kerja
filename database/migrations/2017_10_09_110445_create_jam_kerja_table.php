<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamKerjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jam_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hari')->nullable();
            $table->string('in_time_start')->nullable();
            $table->string('in_time_end')->nullable();
            $table->string('in_time_late')->nullable();
            $table->string('out_time_start')->nullable();
            $table->string('out_time_end')->nullable();
            $table->integer('guru_id')->unsigned();
            $table->foreign('guru_id')->references('id')->on('guru')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jam_kerja');
    }
}
