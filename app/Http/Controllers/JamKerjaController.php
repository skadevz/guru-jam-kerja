<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;
use App\JamKerja;

class JamKerjaController extends Controller
{
    public function create($guru_id)
    {
      $hari = ["Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"];
      $guru = Guru::find($guru_id);
      $jam = JamKerja::where('guru_id', $guru_id)->get();
      return view('jam-kerja.create', ['data' => $jam, 'hari' => $hari, 'guru' => $guru]);
    }

    public function store(Request $r)
    {
      $guru_id = $r->input('guru_id');
      $jam = new JamKerja;
      $jam->hari = $r->input('hari');
      $jam->in_time_start = $r->input('in_time_start');
      $jam->in_time_end = $r->input('in_time_end');
      $jam->in_time_late = $r->input('in_time_late');
      $jam->out_time_start = $r->input('out_time_start');
      $jam->out_time_end = $r->input('out_time_end');
      $jam->guru_id = $guru_id;
      $jam->save();
      return redirect()->route('jam_kerja_create', ['guru_id' => $guru_id]);
    }

    public function delete($id, $guru_id)
    {
      $jam = JamKerja::find($id);
      $jam->delete();
      return redirect()->route('jam_kerja_create', ['guru_id' => $guru_id]);
    }
}
