<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guru;

class GuruController extends Controller
{
    public function index()
    {
      $guru = Guru::orderBy('nama')->paginate(10);
      return view('guru.index', ['data' => $guru]);
    }

    public function create()
    {
      return view('guru.create');
    }

    public function store(Request $r)
    {
      $guru = new Guru;
      $guru->nik = $r->input('nik');
      $guru->nama = $r->input('nama');
      $guru->tempat_lahir = $r->input('tempat_lahir');
      $guru->tanggal_lahir = $r->input('tanggal_lahir');
      $guru->agama = $r->input('agama');
      $guru->save();
      return redirect()->route('guru_index');
    }

    public function edit($id)
    {
      $guru = Guru::find($id);
      return view('guru.edit', ['data' => $guru]);
    }

    public function update(Request $r)
    {
      $id = $r->input('id');
      $guru = Guru::find($id);
      $guru->nik = $r->input('nik');
      $guru->nama = $r->input('nama');
      $guru->tempat_lahir = $r->input('tempat_lahir');
      $guru->tanggal_lahir = $r->input('tanggal_lahir');
      $guru->agama = $r->input('agama');
      $guru->save();
      return redirect()->route('guru_index');
    }

    public function delete($id)
    {
      $guru = Guru::find($id);
      $guru->delete();
      return redirect()->route('guru_index');
    }
}
