<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'GuruController@index')->name('guru_index');
Route::group(['prefix' => 'guru'], function() {
  Route::get('create', 'GuruController@create')->name('guru_create');
  Route::post('store', 'GuruController@store')->name('guru_store');
  Route::get('{id}/edit', 'GuruController@edit')->name('guru_edit');
  Route::post('update', 'GuruController@update')->name('guru_update');
  Route::get('{id}/delete', 'GuruController@delete')->name('guru_delete');
});

Route::group(['prefix' => 'jam-kerja'], function() {
  Route::get('create/{guru_id}/', 'JamKerjaController@create')->name('jam_kerja_create');
  Route::post('store', 'JamKerjaController@store')->name('jam_kerja_store');
  Route::get('delete/{jam_id}/{guru_id}', 'JamKerjaController@delete')->name('jam_kerja_delete');
});
