@extends('app')
@section('content')
  <div class="row row-main">
    <div class="col s12 m12 l12">
      <div class="card-panel">
        <div class="row">
          <div class="col s12 m6 l6">
            <nav class="red nav-breadcrumb">
              <div class="nav-wrapper">
                <div class="col s12 m12 l12">
                  <a href="#" class="breadcrumb">Jam Kerja</a>
                  <a class="breadcrumb">{{ $guru->nama }}</a>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <ul class="collapsible z-depth-0" data-collapsible="accordion">
          @foreach ($data as $key => $value)
            <li>
              <div class="collapsible-header">{{ $value->hari }}</div>
              <div class="collapsible-body">
                <div class="row">
                  <table class="striped">
                    <tbody>
                      <tr>
                        <th>Waktu Absen Masuk</th>
                        <td>{{ $value->in_time_start }}</td>
                      </tr>
                      <tr>
                        <th>Batas Waktu Absen Masuk</th>
                        <td>{{ $value->in_time_end }}</td>
                      </tr>
                      <tr>
                        <th>Waktu Terhitung Terlambat</th>
                        <td>{{ $value->in_time_late }}</td>
                      </tr>
                      <tr>
                        <th>Waktu Absen Pulang</th>
                        <td>{{ $value->out_time_start }}</td>
                      </tr>
                      <tr>
                        <th>Batas Waktu Absen Pulang</th>
                        <td>{{ $value->out_time_end }}</td>
                      </tr>
                      <tr>
                        <th>Aksi</th>
                        <td>
                          <a href="{{ route('jam_kerja_delete', ['jam_id' => $value->id, 'guru_id' => $guru->id]) }}" class="btn waves-effect waves-light red tooltipped" data-position="top" data-delay="50" data-tooltip="Hapus Jam Kerja"><i class="material-icons">delete</i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </li>
          @endforeach
        </ul>
        <div class="row margin-bottom">
          <div class="col s12 m12 l12">
            <h4>Tambah Jam Kerja</h4>
          </div>
        </div>
        <div class="row">
          <form class="col s12 m12 l12" action="{{ route('jam_kerja_store') }}" method="post" autocomplete="off">
            <div class="row margin-bottom">
              {!! csrf_field() !!}
              <input type="hidden" name="guru_id" value="{{ $guru->id }}">
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="in_time_start" name="in_time_start" class="validate @if ($errors->first('in_time_start')) invalid @endif">
                  <label for="in_time_start">Waktu Absen Masuk</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="in_time_end" name="in_time_end" class="validate @if ($errors->first('in_time_end')) invalid @endif">
                  <label for="in_time_end">Batas Waktu Absen Masuk</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <input type="text" id="in_time_late" name="in_time_late" class="validate @if ($errors->first('in_time_late')) invalid @endif">
                  <label for="in_time_late">Waktu Terhitung Terlambat</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="out_time_start" name="out_time_start" class="validate @if ($errors->first('out_time_start')) invalid @endif">
                  <label for="out_time_start">Waktu Absen Pulang</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="out_time_end" name="out_time_end" class="validate @if ($errors->first('out_time_end')) invalid @endif">
                  <label for="out_time_end">Batas Waktu Absen Pulang</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <select class="browser-default" name="hari" id="hari" style="width:100%">
                    <option selected disabled>Hari</option>
                    @foreach ($hari as $key => $value)
                      <option value="{{ $value }}">{{ $value }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <button type="submit" class="btn waves-effect waves-light red">Tambah</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
