<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ url('assets/css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/product-sans.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/icon.css') }}">
    <link rel="stylesheet" href="{{ url('assets/css/custom-style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/plugins/datetimepicker/css/bootstrap-material-datetimepicker.css') }}">
  </head>
  <body class="grey lighten-3">
    <div class="navbar-fixed">
      <nav class="red">
      <div class="nav-wrapper container">
        <a href="{{ url('/') }}" class="brand-logo"><i class="material-icons">list</i>{{ config('app.name') }}</a>
      </div>
    </nav>
    </div>
    <ul id="slide-out" class="side-nav fixed">
      <li class="waves-effect waves-block @if(str_is(url('/'),url()->current())) active @endif">
        <a href="{{ route('guru_index') }}"><i class="material-icons">dashboard</i>Data Guru</a>
      </li>
    </ul>
    <main>
      @yield('content')
    </main>
  </body>
  <script src="{{ url('assets/js/jquery.min.js') }}"></script>
  <script src="{{ url('assets/js/materialize.min.js') }}"></script>
  <script src="{{ url('assets/js/init.js') }}"></script>
  <script src="{{ url('assets/plugins/momentjs/moment-with-locales.min.js') }}"></script>
  <script src="{{ url('assets/plugins/datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
</html>
