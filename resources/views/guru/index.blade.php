@extends('app')
@section('content')
  <div class="row row-main">
    <div class="col s12 m12 l12">
      <div class="card-panel">
        <div class="row">
          <div class="col s12 m4 l4">
            <nav class="red nav-breadcrumb">
              <div class="nav-wrapper">
                <div class="col s12 m12 l12">
                  <a class="breadcrumb">Data Guru</a>
                </div>
              </div>
            </nav>
          </div>
          <div class="col s12 m8 l8">
            <div class="row right">
              <div class="col s12 m12 l12">
                <a href="{{ route('guru_create') }}" class="btn waves-effect waves-light red tooltipped" data-position="top" data-delay="50" data-tooltip="Tambah Data Guru"><i class="material-icons">add_circle</i></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row margin-bottom">
          <div class="col s12 m12 l12">
            <h4>Daftar Guru</h4>
          </div>
        </div>
        <table class="striped responsive-table">
          <thead>
            <tr>
              <th>No</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Agama</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data as $key => $value)
              <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $value->nik }}</td>
                <td>{{ $value->nama }}</td>
                <td>{{ $value->agama }}</td>
                <td>
                  <a href="{{ route('guru_edit', ['id' => $value->id]) }}" class="btn waves-effect waves-light teal tooltipped" data-position="top" data-delay="50" data-tooltip="Ubah Guru"><i class="material-icons">edit</i></a>
                  <a href="{{ route('guru_delete', ['id' => $value->id]) }}" class="btn waves-effect waves-light red tooltipped" data-position="top" data-delay="50" data-tooltip="Hapus Guru"><i class="material-icons">delete</i></a>
                  <a href="{{ route('jam_kerja_create', ['guru_id' => $value->id]) }}" class="btn waves-effect waves-light blue tooltipped" data-position="top" data-delay="50" data-tooltip="Jam Kerja"><i class="material-icons">alarm_add</i></a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
