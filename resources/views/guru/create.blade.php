@extends('app')
@section('content')
  <div class="row row-main">
    <div class="col s12 m12 l12">
      <div class="card-panel">
        <div class="row">
          <div class="col s12 m6 l6">
            <nav class="red nav-breadcrumb">
              <div class="nav-wrapper">
                <div class="col s12 m12 l12">
                  <a href="#" class="breadcrumb">Data Guru</a>
                  <a class="breadcrumb">Tambah</a>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div class="row margin-bottom">
          <div class="col s12 m12 l12">
            <h4>Tambah Data Guru</h4>
          </div>
        </div>
        <div class="row">
          <form class="col s12 m12 l12" action="{{ route('guru_store') }}" method="post" autocomplete="off">
            <div class="row margin-bottom">
              {!! csrf_field() !!}
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="nik" name="nik" class="validate @if ($errors->first('nik')) invalid @endif">
                  <label for="nik">NIK</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="nama" name="nama" class="validate @if ($errors->first('nama')) invalid @endif">
                  <label for="nama">Nama</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="tempat_lahir" name="tempat_lahir" class="validate @if ($errors->first('tempat_lahir')) invalid @endif">
                  <label for="tempat_lahir">Tempat Lahir</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="validate @if ($errors->first('tanggal_lahir')) invalid @endif">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <select class="browser-default" name="agama" id="agama" style="width:100%">
                    <option selected disabled>Agama</option>
                    <option value="Islam">Islam</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Protestan">Protestan</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Budha">Budha</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <button type="submit" class="btn waves-effect waves-light red">Tambah</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
