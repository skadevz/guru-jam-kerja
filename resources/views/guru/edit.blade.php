@extends('app')
@section('content')
  <div class="row row-main">
    <div class="col s12 m12 l12">
      <div class="card-panel">
        <div class="row">
          <div class="col s12 m6 l6">
            <nav class="red nav-breadcrumb">
              <div class="nav-wrapper">
                <div class="col s12 m12 l12">
                  <a href="#" class="breadcrumb">Data Guru</a>
                  <a class="breadcrumb">Ubah</a>
                </div>
              </div>
            </nav>
          </div>
        </div>
        <div class="row margin-bottom">
          <div class="col s12 m12 l12">
            <h4>Ubah Data Guru</h4>
          </div>
        </div>
        <div class="row">
          <form class="col s12 m12 l12" action="{{ route('guru_update') }}" method="post" autocomplete="off">
            <div class="row margin-bottom">
              {!! csrf_field() !!}
              <input type="hidden" name="id" value="{{ $data->id }}">
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="nik" name="nik" class="validate @if ($errors->first('nik')) invalid @endif" value="{{ $data->nik }}">
                  <label for="nik">NIK</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="nama" name="nama" class="validate @if ($errors->first('nama')) invalid @endif" value="{{ $data->nama }}">
                  <label for="nama">Nama</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="tempat_lahir" name="tempat_lahir" class="validate @if ($errors->first('tempat_lahir')) invalid @endif" value="{{ $data->tempat_lahir }}">
                  <label for="tempat_lahir">Tempat Lahir</label>
                </div>
                <div class="input-field col s12 m6 l6">
                  <input type="text" id="tanggal_lahir" name="tanggal_lahir" class="validate @if ($errors->first('tanggal_lahir')) invalid @endif" value="{{ $data->tanggal_lahir }}">
                  <label for="tanggal_lahir">Tanggal Lahir</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <select class="browser-default" name="agama" id="agama" style="width:100%">
                    <option selected disabled>Agama</option>
                    <option value="Islam" @if ($data->agama == "Islam") selected @endif>Islam</option>
                    <option value="Katolik" @if ($data->agama == "Katolik") selected @endif>Katolik</option>
                    <option value="Protestan" @if ($data->agama == "Protestan") selected @endif>Protestan</option>
                    <option value="Hindu" @if ($data->agama == "Hindu") selected @endif>Hindu</option>
                    <option value="Budha" @if ($data->agama == "Budha") selected @endif>Budha</option>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 m12 l12">
                  <button type="submit" class="btn waves-effect waves-light red">Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
